  (function (global, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  } else if (typeof module !== 'undefined' && module.exports){
    module.exports = factory();
  } else {
    global.censorjs = factory();
  }
})(this, function () {
  "use strict";

  var censorjs = function() {};

  var censorChecks = [];
  var grawlixsymbols = ['!','@','#','$','%','&','*'];
  var badWordList = ['cunt', 'dyke', 'fag', 'faggot', 'fuck', 'fuk', 'kike', 'kunt', 'nigger', 'shit', 'gook', 'kyke'];

  function grawlix(word) {
    var grawlixLen = grawlixsymbols.length -1;
    var wordLen = word.length;
    var result = "";
    for (var i = 0; i < wordLen; i++) {
      var pos = Math.floor(Math.random() * grawlixLen);
      result += grawlixsymbols[pos];
    }

    return result;
  }

  censorjs.clean = function(string, replacement) {
    if (replacement == undefined) {
      replacement = grawlix;
    }
    for (var i in censorChecks) {
      string = string.replace(censorChecks[i], replacement);
    }

    return string;
  };

  censorjs.setWordList = function(wordList) {
    censorChecks = [];
    generateCensorChecks(wordList);
  };

  function generateCensorChecks(badwords) {

    // generate censor checks as soon as we load the dictionary
    // utilize leet equivalents as well
    var leet_replace = [];
    leet_replace.a = '(a|a\.|a\-|4|@|\u00c1|\u00e1|\u00c0|\u00c2|\u00e0|\u00c2|\u00e2|\u00c4|\u00e4|\u00c3|\u00e3|\u00c5|\u00e5|\u03b1|\u0394|\u039b|\u03bb)';
    leet_replace.b = '(b|b\.|b\-|8|\|3|\u00df|\u0392|\u03b2)';
    leet_replace.c = '(c|c\.|c\-|\u00c7|\u00e7|\u00a2|\u20ac|<|\\(|{|\u00a9)';
    leet_replace.d = '(d|d\.|d\-|&part;|\\|\\)|\u00de|\u00fe|\u00d0|\u00f0)';
    leet_replace.e = '(e|e\.|e\-|3|\u20ac|\u00c8|\u00e8|\u00c9|\u00e9|\u00ca|\u00ea|\u2211)';
    leet_replace.f = '(f|f\.|f\-|\u0192)';
    leet_replace.g = '(g|g\.|g\-|6|9)';
    leet_replace.h = '(h|h\.|h\-|\u0397)';
    leet_replace.i = '(i|i\.|i\-|!|\||\]\[|]|1|\u222b|\u00cc|\u00cd|\u00ce|\u00cf|\u00ec|\u00ed|\u00ee|\u00ef)';
    leet_replace.j = '(j|j\.|j\-)';
    leet_replace.k = '(k|k\.|k\-|\u039a|\u03ba)';
    leet_replace.l = '(l|1\.|l\-|!|\||\]\[|]|\u00a3|\u222b|\u00cc|\u00cd|\u00ce|\u00cf)';
    leet_replace.m = '(m|m\.|m\-)';
    leet_replace.n = '(n|n\.|n\-|\u03b7|\u039d|\u03a0)';
    leet_replace.o = '(o|o\.|o\-|0|\u039f|\u03bf|\u03a6|\u00a4|\u00b0|\u00f8)';
    leet_replace.p = '(p|p\.|p\-|\u03c1|\u03a1|\u00b6|\u00fe)';
    leet_replace.q = '(q|q\.|q\-)';
    leet_replace.r = '(r|r\.|r\-|\u00ae)';
    leet_replace.s = '(s|s\.|s\-|5|\$|\u00a7)';
    leet_replace.t = '(t|t\.|t\-|\u03a4|\u03c4)';
    leet_replace.u = '(u|u\.|u\-|\u03c5|\u00b5)';
    leet_replace.v = '(v|v\.|v\-|\u03c5|\u03bd)';
    leet_replace.w = '(w|w\.|w\-|\u03c9|\u03c8|\u03a8)';
    leet_replace.x = '(x|x\.|x\-|\u03a7|\u03c7)';
    leet_replace.y = '(y|y\.|y\-|\u00a5|\u03b3|\u00ff|\u00fd|\u0178|\u00dd)';
    leet_replace.z = '(z|z\.|z\-|\u0396)';

    for (var x=0; x<badwords.length; x++) {
      var badWordLen = badwords[x].length;
      var regex = "";
      for (var i = 0; i < badWordLen; i++) {
        regex += leet_replace[badwords[x][i]];
      }

      censorChecks.push(new RegExp(regex, 'gi'));
    }
  }

  generateCensorChecks(badWordList);

  return censorjs;
});

