# censorjs

[![Build Status](https://drone.io/bitbucket.org/muxy/censorjs/status.png)](https://drone.io/bitbucket.org/muxy/censorjs/latest)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg?style=flat-square)](http://www.gnu.org/licenses/agpl-3.0)
[![NPM](https://img.shields.io/npm/dt/censorjs.svg?style=flat-square)](https://www.npmjs.com/package/censorjs)

Javascript profanity filter. Uses regex to filter out replacements and derivatives.

Based on [BanBuilder](https://github.com/snipe/banbuilder)


### Using
```
var censorjs = require('censorjs');

var cleaned = censorjs.clean("String with dirty words here :(");
console.log(cleaned);

```